%define byaccdate 20210808

Name:           byacc
Version:        2.0.%{byaccdate}
Release:        6
Summary:        A parser generator
License:        public domain
URL:            https://invisible-island.net/byacc/byacc.html
Source0:        https://invisible-mirror.net/archives/byacc/byacc-%{byaccdate}.tgz

Patch9000:	CVE-2021-33641.patch
Patch9001:	CVE-2021-33642.patch

BuildRequires:  gcc

%description
Berkeley Yacc is an LALR(1) parser generator.  Berkeley Yacc has been made
as compatible as possible with AT&T Yacc.  Berkeley Yacc can accept any input
specification that conforms to the AT&T Yacc documentation.  Specifications
that take advantage of undocumented features of AT&T Yacc will probably be
rejected.

%package_help

%prep
%autosetup -n byacc-20210808 -p1
find . -type f -name \*.c -print0 | xargs -0 sed -i 's/YYSTACKSIZE 500/YYSTACKSIZE 10000/g'

%build
%configure --disable-dependency-tracking
%make_build

%install
%make_install
ln -s yacc %{buildroot}%{_bindir}/byacc
ln -s yacc.1 %{buildroot}%{_mandir}/man1/byacc.1

%check
%make_build check

%files
%doc ACKNOWLEDGEMENTS README* NO_WARRANTY
%license AUTHORS
%{_bindir}/*

%files help
%doc CHANGES NOTES
%{_mandir}/man1/*

%changelog
* Fri Feb 17 2023 tanyulong <tanyulong@kylinos.cn> - 2.0.20210808-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:optimize test command

* Mon Dec 26 2022 zhoupengcheng <zhoupengcheng11@huawei.com> - 2.0.20210808-5
- fix CVE-2021-33641 and CVE-2021-33642

* Sat Dec 24 2022 chenmaodong <chenmaodong@xfusion.com> - 2.0.20210808-4
- Modify changelog error

* Mon Nov 21 2022 yaoxin <yaoxin30@h-partners.com> - 2.0.20210808-3
- Modify invalid Source

* Wed Jun 29 2022 wangjiang <wangjiang37@h-partners.com> - 2.0.20210808-2
- DESC:enable check test suite

* Sat Dec 25 2021 tianwei<tianwei12@huawei.com> - 2.0.20210808
- DESC:upgrade to 2.0.20210808

* Mon Feb 1 2021 wangjie<wangjie294@huawei.com> - 2.0.20210109
- DESC:upgrade 2.0.20210109

* Sat Jul 25 2020 xinghe <xinghe1@huawei.com> - 1.9.20200330-1
- update version to 1.9.20200330

* Wed Jan 22 2020 gulining<gulining1@huawei.com> - 1.9.20170709-9
- Disable test

* Fri Dec 6 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.9.20170709-8
- Add help package

* Wed Dec 4 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.9.20170709-7
- Package init
